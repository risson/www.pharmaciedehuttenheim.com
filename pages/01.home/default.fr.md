---
title: Accueil
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

# Bienvenue
## sur le site de la **Pharmacie de Huttenheim**

___

<div align="center"><div markdown="1">
![Devanture](/images/devanture-pharmacie-huttenheim.jpg?quality=100)
</div></div>

___

Votre pharmacie est ouverte :
* Du **lundi au vendredi** de **8:30 à 12:30** et de **14:00 à 19:00**
* Le **samedi en continu** de **08:00 à 15:00**

Téléphone : [03.88.74.32.97](tel:0388743297)  
Adresse : 19 Rue de Benfeld 67230 Huttenheim  
[**En savoir plus**](/infos-utiles/nous-contacter-et-nous-trouver)  
  
[fa=fa-facebook-square /] Retrouvez les dernières actualités de la pharmacie sur [notre page Facebook](https://www.facebook.com/pharmaciedehuttenheim).