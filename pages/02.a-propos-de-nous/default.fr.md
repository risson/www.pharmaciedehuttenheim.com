---
title: 'À propos de nous'
media_order: '49790996_2066851270060164_2992359742971576320_o.jpg,IMG_3331.jpg'
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

# À propos de nous  
  
### Notre équipe

![](IMG_3331.jpg)

L’équipe de la Pharmacie de Huttenheim est composée de 7 membres :

* Mme SCHMITT Emmanuelle, titulaire de la Pharmacie de Huttenheim, docteur en pharmacie, diplômée de la Faculté de Pharmacie de Strasbourg
* Mme BIBLER Laure, pharmacien adjoint, docteur en pharmacie, diplômée de la Faculté de Pharmacie de Strasbourg
* Mme KAVALA Céline, pharmacien adjoint, docteur en pharmacie, diplômée de la Faculté de Pharmacie de Strasbourg
* Mme LIBS Viviane, pharmacien adjoint, docteur en pharmacie, diplômée de la Faculté de Pharmacie de Strasbourg, titulaire d'un Diplôme Universitaire spécialité phytothérapie et aromathérapie
* Mme ECKERT Nadine, préparatrice en pharmacie
* Mme BOOTZ Laetitia, préparatrice en pharmacie
* Mme SONDEJ Tiffany, préparatrice en pharmacie, titulaire d'un Diplôme Universitaire spécialité vétérinaire

### Nos compétences

Toute l’équipe de la Pharmacie de Huttenheim est titulaire des diplômes requis pour une prise en charge qualifiée, professionnelle et compétente. Elle se forme régulièrement pour renouveler ses connaissances dans le domaine scientifique qui évolue sans cesse.

Nos compétences vont de la dispensation des spécialités pharmaceutiques jusqu’à l’éducation thérapeutique du patient, en passant par des domaines tels que :
* l’aromathérapie, la phytothérapie, l’homéopathie, la micronutrition, la diététique
* l’automédication, la petite enfance
* la contention, l’orthopédie, le maintien à domicile
* la dermo-cosmétique
* le vétérinaire.

Nous parlons français, alsacien, allemand et anglais.

### Nos services

Vous pouvez profiter des nombreux services que nous vous proposons, tel que :

* entretien pharmaceutique : suivi AVK, suivi asthme…
* dépistage de neuropathies végétatives et suivi chez les diabétiques
* vente et location de matériel médical
* location de tire-lait
* maintien à domicile
* bilan de santé (demandé par les complémentaires)
* conseil personnalisé en micro-nutrition, en observance
* service de garde
* [Ma Pharmacie Mobile](http://www.mapharmaciemobile.com/) + accès à l’information
* espace de confidentialité
* espace orthopédie
* borne de mise à jour de carte Vitale
* accès au Dossier Pharmaceutique (DP)
* collecte des Déchets d’Activités de Soins à Risques infectieux ([DASTRI](https://www.dastri.fr/)) et des médicaments non utilisés, périmés ou non ([CYCLAMED](http://www.cyclamed.org/))
* distributeur extérieur de préservatifs
* livraison à domicile
* parking
* place personne à mobilité réduite
* pharmacie climatisée