---
title: Évènements
published: false
twitterenable: true
twittercardoptions: summary
facebookenable: true
article:
    '@context': 'http://schema.org/'
    '@type': NewsArticle
    image:
        '@type': ImageObject
    publisher:
        '@type': Organization
        logo:
            '@type': ImageObject
musicevent:
    '@context': 'http://schema.org/'
    '@type': MusicEvent
    location:
        '@type': MusicVenue
    offers:
        '@type': Offer
        priceCurrency: AED
person:
    '@context': 'http://schema.org/'
    '@type': person
    address:
        '@type': PostalAddress
event:
    '@context': 'http://schema.org/'
    '@type': Event
    location:
        '@type': Place
musicalbum:
    '@context': 'http://schema.org/'
    '@type': MusicAlbum
    byArtist:
        '@type': MusicGroup
restaurant:
    '@context': 'http://schema.org/'
    '@type': Restaurant
    address:
        '@type': PostalAddress
---

![Conférence Micro-Nutrition](conference_micro_nutrition.jpg?resize=600,848)  
Voici venue la conférence que beaucoup d'entre vous attendiez...  
En effet, l'alimentation est un sujet passionnant qui nous concerne tous et ô combien important pour notre santé.  
Venez y assister dans votre Pharmacie le Mardi 31 octobre à 20h00.  
Sachant que vous serez nombreux, veuillez réserver votre place car notre capacité d'accueil est limitée.  
N'hésitez pas à [nous contacter](http://www.pharmaciedehuttenheim.com/infos-utiles/nous-contacter-et-nous-trouver) dès maintenant.  

Plus d'informations sur nos évènements à venir sur notre [page Facebook](https://www.facebook.com/pharmaciedehuttenheim/).