---
title: 'Nos produits'
media_order: 'dmedica-logo.jpg,fleurs-bach.png,igloo.jpg,neut.jpg,thuane_sport.jpg,avene_bb.jpg,avent.jpg,biarritz.jpg,eg_granions.jpg,guigoz.jpg,mam.jpg,novalac.jpg,physiolac.jpg,pranarom.jpg,quies.jpg,renu.jpg,sonalto.jpg,therapearl.jpg,weleda.jpg,zeiss.jpg,3m.jpg,coloplast.jpg,gilbert.jpg,mepilex.jpg,urgo.jpg,boiron.jpg,colgate.jpg,gsk.jpg,inava.jpg,johnson_johnson.jpg,procter_gamble.jpg,sunstar.jpg,biosynex.jpg,apivita.jpg,laino.jpg'
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

# Nos produits

Retrouvez la liste des laboratoires avec qui nous travaillons, dans les domaines suivants :

* [Matériel médical](#materiel-medical)
* [Médecine naturelle](#medecine-naturelle)
* [Beauté](#beaute)
* [Bébé](#bebe)
* [Vétérinaire](#veterinaire)
* [Location](#location)
* [Vision - Audition](#vision-audition)
* [Pansements - Soins](#pansements-soins)
* [Hygiène bucco-dentaire](#hygiene-bucco-dentaire)
* [Diagnostic](#diagnostic)

### <div id="materiel-medical"></div>Matériel médical

|   |   |   |   |   |
|:-:|:-:|:-:|:-:|:-:|
| [![Sigvaris](/images/logos/sigvaris.png)](http://www.sigvaris.com/france/fr-fr) | [![Gibaud](/images/logos/gibaud.png)](https://www.gibaud.com/FR/) | [![Innothera](/images/logos/innothera.png)](http://www.innothera.fr/fr/) | [![Thuasne](/images/logos/thuasne.png)](http://www.thuasne.com/) | [![Donjoy](/images/logos/donjoy.png)](http://www.djoglobal.eu/fr_FR/DonJoy.html) |
| [![DMedica](dmedica-logo.jpg?resize=400,278)](http://www.dmedica.com) | [![neut](neut.jpg)](https://www.neut.fr/fr/accueil.html) | [![Thuane Sport](thuane_sport.jpg)](https://www.thuasnesport.com/) | ![igloo](igloo.jpg) |  |

### <div id="medecine-naturelle"></div>Médecine naturelle

|   |   |   |   |   |
|:-:|:-:|:-:|:-:|:-:|
| [![Arkopharma](/images/logos/arkopharma.png)](http://www.arkopharma.fr/) | [![Boiron](/images/logos/boiron.jpg)](http://www.boiron.fr/) | [![Fleurs de Bach](fleurs-bach.png)](https://fleursdebach.fr/) | [![Aroma](/images/logos/aroma.png)](https://www.lecomptoiraroma.fr/) | [![Pileje](/images/logos/pileje.png)](http://www.pileje-micronutrition.fr/) |
| [![Santé Verte](/images/logos/sante-verte.png)](https://www.sante-verte.com/fr/) | [![Soria](/images/logos/soria.png)](http://www.soriavie.fr/presentation/) | ![Puressentiel](/images/logos/puressentiel.png) | ![Weleda](/images/logos/weleda.jpg) | [![Mediflor](/images/logos/mediflor.png)](https://www.infusion-mediflor.fr/) |
| [![Lehning](/images/logos/lehning.png)](http://www.lehning.com/fr) | [![Pranarôm](pranarom.jpg)](https://www.pranarom.com/fr/accueil) | [![Laboratoire des Granions](eg_granions.jpg)](https://www.granions.fr/) | [![Laino](laino.jpg)](https://www.laino.fr/) | ![Apivita](apivita.jpg) |

### <div id="beaute"></div>Beauté

|   |   |   |   |
|:-:|:-:|:-:|:-:|
| [![A-Derma](/images/logos/a-derma.png)](https://www.aderma.fr/fr-fr) | [![Avène](/images/logos/avene.png)](https://www.eau-thermale-avene.fr/) | [![Ducray](/images/logos/ducray.png)](https://www.ducray.com/fr-fr/) | [![Laino](/images/logos/laino.png)](http://www.laino.fr/) |
| [![Nuxe](/images/logos/nuxe.png)](https://fr.nuxe.com/) | [![Roge Cavailles](/images/logos/roge-cavailles.jpg)](http://www.rogecavailles.fr/) | ![Weleda](/images/logos/weleda.jpg) | ![La Roche Posay](/images/logos/la-roche-posay.png) |

### <div id="bebe"></div>Bébé

|   |   |   |   |   |
|:-:|:-:|:-:|:-:|:-:|
| [![Mustela](/images/logos/mustela.png)](http://www.mustela.fr/) | [![Gallia](/images/logos/gallia.png)](https://www.laboratoire-gallia.com/) | [![Gifrer](/images/logos/gifrer.jpg)](http://www.gifrer.fr/) | [![Gilbert](/images/logos/gilbert.png)](http://www.labogilbert.fr/) | [![A-Derma](/images/logos/a-derma.png)](https://www.aderma.fr/fr-fr) |
| ![Weleda](/images/logos/weleda.jpg) | [![Luc et Léa](/images/logos/luc-et-lea.png)](https://www.luc-et-lea.fr/) | [![Guigoz](guigoz.jpg)](https://www.guigoz.fr/) | ![MAM](mam.jpg) | [![Avène Pediatril](avene_bb.jpg)](https://www.eau-thermale-avene.fr/pediatril) |
| [![Novalac](novalac.jpg)](https://www.laboratoires-novalac.fr) | [![Physiolac](physiolac.jpg)](https://www.physiolac.fr/) | ![AVENT](avent.jpg) | ![Laboratoires de Biarritz](biarritz.jpg) |  |

### <div id="veterinaire"></div>Vétérinaire

[![Clément-Thekan](/images/logos/clement-thekan.jpg)](http://www.omega-pharma.fr/produit.php?docid=146)

### <div id="location"></div>Location

La Pharmacie de Huttenheim propose à la location les appareils suivants :

* Tire lait
* Aérosols
* Lits médicalisés
* Fauteils roulants
* Appareillage hospitalisation à domicile
* Pèse bébé
* Tens
* Neurostimulateur

### <div id="vision-audition"></div>Vision - Audition

|   |   |   |
|:-:|:-:|:-:|
| ![Quies](quies.jpg) | [![RENU](renu.jpg)](http://www.bausch.com/) | [![Sonalto](sonalto.jpg)](https://www.sonalto.fr) |
| [![Thera Pearl](therapearl.jpg)](http://www.bausch.ca/fr-ca/nos-produits/gouttes-ophtalmiques-gels-et-onguents/yeux-secs/masque-pour-les-yeux-therapearl#.XHJ2iqJKhEY) | ![Weleda](weleda.jpg) | [![Zeiss](zeiss.jpg)](http://www.omega-pharma.fr/) |

### <div id="pansements-soins"></div>Pansements - Soins

|   |   |   |
|:-:|:-:|:-:|
| [![3M](3m.jpg)](https://www.3mfrance.fr/3M/fr_FR/notre-societe-fr/tous-les-produits-3M/~/Tous-les-produits-3M/Pansement-standard/?N=5002385+8711017+8717839&rt=r3) | [![Coloplast](coloplast.jpg)](https://www.coloplast.fr/) | [![Gilbert](gilbert.jpg)](https://www.labogilbert.fr/) |
| ![Mepilex](mepilex.jpg) | [![Urgo](urgo.jpg)](http://www.urgo.fr/) |  |

### <div id="hygiene-bucco-dentaire"></div>Hygiène bucco-dentaire

|   |   |   |   |
|:-:|:-:|:-:|:-:|
| [![Boiron](boiron.jpg)](https://www.boiron.fr/nos-produits/decouvrir-nos-produits/et-aussi/homeodent-soin-gencives-sensibles) | [![Colgate](colgate.jpg)](https://www.colgate.fr/) | [![GSK](gsk.jpg)](https://fr.gsk.com/) | [![Inava](inava.jpg)](https://fr-fr.pierrefabre-oralcare.com) |
| ![Johnson & Johnson](johnson_johnson.jpg) | ![Procter & Gamble](procter_gamble.jpg) | [![Sunstar](sunstar.jpg)](https://www.sunstargum.com/fr/) | ![Weleda](weleda.jpg) |

### <div id="diagnostic"></div>Diagnostic

[![Biosynex](biosynex.jpg)](https://www.biosynex.com/)

* Tests : grossesse, ovulation, infection vaginale, mycose vaginale, infection urinaire, gluten, VIH, fertilité masculine, THC (cannabis)
* Tensiomètre
* Thermomètre
* Lecteur de glycémie
