---
title: 'Miel de l''Hiver - Novembre 2020'
media_order: 'Miel de l''hiver-1.jpg'
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

![](Miel%20de%20l'hiver-1.jpg)