---
title: 'Ravintsara - Octobre 2020'
media_order: huile-essentielle-ravintsara-bio.jpg
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

# Ravintsara (cinnamomum camphora)

![Ravintsara](huile-essentielle-ravintsara-bio.jpg?resize=200,200)

Tous les mois, la Pharmacie de Huttenheim vous présente une huile essentielle.
Ce mois-ci : le Ravintsara (cinnamomum camphora).
N'hésitez pas à consulter [nos conseils](https://www.pharmaciedehuttenheim.com/nos-conseils) sur l'utilisation des huiles essentielles.

### [fa=fa-exclamation-triangle /] Précautions d'emploi

Déconseillée aux femmes enceintes ou allaitantes et aux enfants de moins de 3 mois.

### Quelles propriétés

Reine de l'hiver, on l'utilise pour son excellent pouvoir __antiviral__, et ses effets stimulants sur le système immunitaire. Elle est également antibactérienne et expectorante.

### On l'utilise en cas de :

* Grippe, épidemies virales ORL (curatif) :	
  + Voie cutanée : 2 à 3 gouttes sur le bas du dos, le long de la colonne vertébrale et sur le thorax. 4 à 5 fois par jour jusqu'à amélioration
  + Voie orale : 1 goutte sur un comprimé neutre ou dans du miel. 4 fois par jour
* Arrivée du froid, renforcement de l'immunité (prévention) :
  + Voie cutanée : 2 à 3 gouttes sur la face interne des poignets et respirer profondément. 1 fois pour jour. Faire une pause d'une semaine après 3 semaines d'utilisation

### Recette

Huile essentielle de Ravinstara, d'Eucalyptus radié et de Tea tree : 4 gouttes de chaque en diffusion 20 à 30 minutes par heure

### DIY - Do It Yourself

Vous avez envie de faire votre spray assainissant vous-même ? Pour cela, c’est tout simple, il suffit de mélanger :

* 300 gouttes d’huile essentielle de ravintsara
* 200 gouttes d’huile essentielle de citron
* 80 gouttes d’huile essentielle de lavande vraie

Le tout dans un flacon « spray » de 30mL. Puis, vaporiser dans l’air 1 à 2 pulvérisations 3 fois par jour.

Pour les contre-indications, n’hésitez pas à demander conseil à votre équipe officinale qui se fera un plaisir de vous répondre.

![](Formule%20spray%20assainissant.PNG)

## Comment utiliser les huiles essentielles ?

* [fa=fa-glass /] Voie orale : jamais pure (risque de brûlure). A prendre sur un comprimé neutre. 1 à 2 gouttes 3 fois par jour (6-8 gouttes max/jour)
* [fa=fa-tint /] Massage : soit pure (pour un effet plus prononcé), soit à mélanger avec une huile végétale. 8 gouttes diluées dans 5ml d'huile végétale
* [fa=fa-bath /] Bain : jamais pure (risque de brûlure). A mélanger avec la base neutre. 20 à 30 gouttes diluées dans une base neutre
* [fa=fa-cloud /] Diffusion : avec un diffuseur électrique. Inhalation (humide ou sèche). 30 gouttes, 15 à 20 minutes de diffusion

### [fa=fa-exclamation-triangle /] Les précautions d'emploi

Pour les enfants de moins de 6 ans, les femmes enceintes ou allaitantes, les asmathiques et épiléptiques, et sur les yeux et les muqueuses, demander l'avis d'un expert.

#### Ayez le bon réflexe

Ingestion accidentelle => Avaler quelques cuillerées d'huile végétale  
Brûlure de peau, allergie cutanée => Rincer avec de l'huile végétale
