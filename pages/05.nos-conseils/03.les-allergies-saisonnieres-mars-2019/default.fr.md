---
title: 'Les allergies saisonnières - Mars 2019'
media_order: 'allergies_saisonnieres.jpg,allergie_symptomes.jpg'
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

# Les allergies saisonnières - Mars 2019

[center]
![](allergies_saisonnieres.jpg)
[/center]

### Trois grandes périodes

| Janvier à Avril | Mai à Juillet | Juillet jusqu'à l'Automne |
|-----------------|---------------|---------------------------|
| **arbres**      | **graminées** | **herbacés**              |
| cyprès          | fourragères   | armoise                   |
| noisetiers      | céréalières   | pissenlit                 |
| aulnes          |               |                           |
| charme          |               |                           |

[Carte des pollens](https://www.pollens.fr/)

### Origine

C’est un dysfonctionnement du système immunitaire causé par les lymphocytes T qui prennent pour cible les substances non pathogènes (pollens, poussières, acariens, moisissures) pour pathogènes. **C’est la réaction allergique.**

### Symptômes

![](allergie_symptomes.jpg)

* Sensation de nez bouché, démangeaisons, nez qui coule clair, éternuements. **C’est la rhinite allergique ou rhume des foins.**
* Larmoiements, paupières gonflées. **C’est la conjonctivite allergique.**
* Difficultés à respirer. **C’est l’asthme allergique.**

Cette allergie saisonnière peut se traduire également par de l’urticaire, de l’eczéma atopique ou de contact. L’urgence médicale étant l’œdème de QUINKE avec des lèvres gonflées et/ou des paupières gonflées, gorge serrée.

### Traitement

* antihistaminique par voie orale,
* lavage du nez (eau de mer ou sérum physiologique), 
* spray nasal, collyres (anti-allergique).

<i class="fa fa-warning"></i> Si infection consulter son médecin.

### Les solutions aromathérapiques

#### En diffusion

Mélange de 15 gouttes de chaque huile : Estragon et lavande officinale.

En diffusion 1 heure avant le coucher (sans la présence de la personne allergique). Diffusion pendant 15 jours. Plusieurs diffusions par jour sont possibles.

***Ne pas diffuser en présence d’enfant de moins de 12 ans.***

Variante : +15 gouttes de pin des montagnes

#### Par voie orale

*Pour les personnes âgées de plus de 6 ans*

Une goutte d’huile essentielle de lavande officinale sur un comprimé neutre 3 fois par jour pendant 15 jours durant la période à risque. Observer à l’issue des 15 jours un arrêt de 7 jours.

### Les solutions phytothérapique

Plantain associé au cassis.

Bouillon blanc.

### Les solutions homéopathiques

* Euphrasia (yeux)
* Sabadilla (nez) : éternuements spasmodiques
* Pollen 30CH – 5 granules par jour (rhume des foins)
* Poumon histamine 9CH - 5 granules 2 fois par jour pendant toute la saison allergique
* Apis melifica 15CH – 5 granules 3 fois par jour (nez bouché – démangeaisons)
* Nux vomica 5CH (éternuements au réveil + nez bouché)

### Conseils préventifs

1. Pulvériser un spray nasal en poudre (poudre de cellulose d’origine végétale et poudre de menthe poivrée) dans le nez qui forme une barrière naturelle contre les allergènes présents dans l’air.
2. **Micronutrition** : Gamme PILEJE allergie 
3. **Par voie cutanée** : Mélanger une goutte d’huile essentielle d’estragon et de lavandin aspic et une huile végétale, puis appliquer sur les ailettes du nez.
4/ **Homéopathie** : Pollens 30CH et poumon histamine 15CH : une dose de chaque par semaine et ce pendant un à deux mois avant la période des allergies.

### Conseils pratiques

Ne pas ouvrir les fenêtres aux heures ou le pollen est présent au maximum (tôt le matin et tard le soir)  
Ne pas tondre la pelouse  
Ne pas étendre son linge dehors  
Se doucher (corps et cheveux) avant de se coucher  
Procéder au lavage du nez et des yeux  
Changer les filtres à air ou climatisation des voitures  
Ne pas pratiquer d’activité physique à l’extérieur  
Eviter les parcs  
Rouler les fenêtres de la voiture fermées  
Porter un masque  