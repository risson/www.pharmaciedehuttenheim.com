---
title: 'Huiles essentielles, pour votre santé et votre bien-être - Février 2019'
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

# Huiles essentielles, pour votre santé et votre bien-être

![](fev2019.jpg)

### De la phytothérapie à l'aromathérapie

#### **Une démarche active au profit de la santé et du bien-être**

Être bien dans son corps, bien dans sa tête : ce souhait bien légitime, que nous partageons tous, dépend pour beaucoup de nous-mêmes. Santé physique et mentale, confort et bien-être, passent d’abord par l’hygiène de la vie et la prévention. Se nourrir de manière équilibrée, s’octroyer le sommeil suffisant, pratiquer l’activité physique nécessaire, éviter les excès… voilà les bases d’une démarche responsable, dictée par le simple bon sens. Plutôt prévenir que guérir, l’adage est vieux comme le monde… et plus que jamais d’actualité.

Revenir aux fondamentaux de la santé, c’est aussi faire appel aux solutions durables. Plongés dans un environnement de plus en plus artificiel, soumis à des tensions croissantes, exposés à des pollutions et à des nuisances de toutes sortes, nos organismes malmenés ont besoin de retrouver leur équilibre au travers de produits d’origine naturelle, dont l’action se diffuse harmonieusement, dans le respect des rythmes biologiques.

#### **Pour une pratique simple et sûre**

Source de médicaments et de produits de santé, le monde végétal offre un potentiel inépuisable d’actifs naturels, capables de soulager nos troubles fonctionnels, de rééquilibrer nos systèmes organiques, de nous défendre contre les agressions, de restaurer notre énergie vitale.

Dans le domaine de la **phytothérapie**, les actifs végétaux sont recueillis au terme d’un processus complexe qui permet de transformer la plante en extraits concentrés, secs ou fluides. On les trouve en pharmacie sous forme de gélules ou de sachets-sticks.

Dans le domaine de l’**aromathérapie**, ce sont les molécules volatiles de la plante qui sont libérées par le procédé de la distillation, sous une forme liquide et concentrée : l’**huile essentielle**.

En privilégiant la plante dans tous ses états les deux approches se complètent efficacement. Comme la phytothérapie, l’aromathérapie procède d’une démarche active et réfléchie. En présence d’un problème de santé, la prise d’une huile essentielle dans un but curatif doit être cautionnée par le médecin ou par le pharmacien. Dans le cadre quotidien l’aromathérapie peut se pratiquer aisément dans un but de confort et de bien-être.

L’objectif des explications à suivre est de vous donner les clés d’une utilisation simple et sûre des huiles essentielles, adaptée à vos attentes personnelles et à vos besoins familiaux.

### Huiles essentielles : comment les utiliser

Respectez le mode d’utilisation conseillé, ainsi que les précautions générales et particulières. N’oubliez pas que certaines HE sont réservées à l’usage des adultes.

N’augmentez pas la dose sans l’avis d’un médecin (spécialisé en aromathérapie) ou d’un pharmacien. Consultez si vous n’obtenez pas d’amélioration après 48 heures.

Quelle que soit la voie d’absorption, ne prenez pas plus de 2 ou 3 huiles essentielles en même temps.

#### **Précautions générales**

* pas d’utilisation chez la femme enceinte ou allaitante ;
* pas d’utilisation chez l’enfant de moins de 7 ans sans avis médical ;
* pas d’utilisation en cas d’épilepsie, d’allergie aux molécules aromatiques ;
* personnes âgées : sur avis médical ;
* ne pas utiliser en continu ;
* ne pas utiliser une huile essentielle pure, sauf mention contraire ;
* ne pas l’appliquer près des yeux, dans les oreilles ou le nez (sauf indication spécifique ;
* se laver les mains à l’eau et au savon après une application ;
* ne pas diffuser en continu, ni en présence d’un jeune enfant ;
* ne pas laisser à la portée des enfants.

-----

###### 1/ Par voie orale

**Dans quel cas ?** La voie orale est généralement utilisée chez l’adulte et conseillée lors de troubles digestifs et de manifestations d’origine infectieuse. Cette voie d’absorption est contre-indiquée en cas de gastrite, d’ulcère gastro-duodénal, de reflux œsophagien. Incompatibilité possible avec certains traitements. La prise d’un autre médicament devra être éloignée de 2 à 3 heures. Demandez l’avis de votre médecin ou de votre pharmacien.

**Comment ?** Les huiles essentielles ne doivent jamais être mises pures dans la bouche. Elles ne sont pas solubles dans l’eau. Prenez-les de préférence dans une cuillerée à café d’huile de cuisine (olive, tournesol, …) ou de teinture-mère (en pharmacie). Dans quelques cas, il est indiqué de déposer les gouttes sur un morceau de sucre, de pain de mie ou un comprimé neutre ; buvez ensuite un verre d’eau (tiède si possible).

-----

###### 2/ Par voie cutanée

Les huiles essentielles traversent facilement les couches cutanées pour rejoindre la circulation sanguine. Leur action est locale ou générale. Certaines peuvent être irritantes et même dermocaustiques. Dans tous les cas, éviter les zones sensibles et les muqueuses.

**Dans quel cas ?** La voie cutanée convient dans de nombreuses situations. Elle offre des possibilités très agréables pour les soins corporels et cosmétiques.

**Comment ?** Les huiles essentielles s’utilisent le plus souvent en dilution dans une huile végétale. Vous les appliquerez en massage, exceptionnellement en friction, soit à proximité de la zone concernée, soit sur un centre nerveux (plexus solaire, colonne vertébrale,…), soit sur une zone bien vascularisée : poignet, pli du coude ou du genou, plante des pieds…

### Huiles végétales : le meilleur véhicule des actifs

Trop puissantes pour être appliquées pures, les huiles essentielles s’utilisent diluées (sauf exception) dans un liquide adapté : huile végétale de Macadamia, Amande, Argan, Calophylle ou Rose musquée, cire de Jojoba, Arnica, Avocat, Noyau d’Abricot…

Les huiles végétales favorisent la diffusion des huiles essentielles au travers de la peau. Très riches en acides gras, elles renforcent le film protecteur de l’épiderme et combattent le dessèchement cutané.

Issues de culture biologique, les huiles végétales se conservent 30 mois à l’abri de la lumière, de l’air et de la chaleur. Après ouverture du flacon, utiliser dans les 6 mois.

**Tableau des dilutions** : nombre de gouttes d’huile essentielle selon le pourcentage de dilution et la taille du flacon utilisé pour réaliser le mélange.

|                    | **Flacon de 10 ml** | **Flacon de 30 ml** | **Flacon de 50 ml** |
|--------------------|---------------------|---------------------|---------------------|
| **Dilution à 2%**  | 5 gouttes           | 15 gouttes          | 25 gouttes          |
| **Dilution à 5%**  | 12 gouttes          | 37 gouttes          | 62 gouttes          |
| **Dilution à 10%** | 25 gouttes          | 75 gouttes          | 125 gouttes         |
| **Dilution à 15%** | 37 gouttes          | 112 gouttes         | 187 gouttes         |

-----

###### 3/ En diffusion atmosphérique

Certaines huiles essentielles s’utilisent pures dans un diffuseur électrique par nébulisation à froid. Cette technique est la plus efficace pour assurer la diffusion des huiles essentielles. Sans être chauffées, les huiles essentielles sont projetées en minuscules gouttelettes et se dispersent dans l’atmosphère (surface de 20 à 100 m² environ). Dans ce mode d’utilisation, la plupart des huiles essentielles sont réservées aux adultes ; les autres modes d’utilisation ne sont indiqués qu’aux enfants de plus de 7 ans.

**Dans quel cas ?** La diffusion atmosphérique convient particulièrement aux huiles essentielles à visée atmosphérique ou relaxante. C’est la voie idéale pour assainir les pièces à vivre… et pour se détendre.

**Comment ?** La diffusion ne se pratique jamais en continu. Rythme conseillé :

* dans une pièce vide, 15 à 20 minutes deux à trois fois par jour ;
* dans une pièce occupée, 5 à 10 minutes toutes les heures ou les deux heures.

Quantité pour 24 heures : 20 à 30 gouttes (en fonction du débit de l’appareil).

**Bon à savoir** : quelques huiles essentielles exaltant une odeur trop forte (risque de maux de tête), sont à diffuser avec modération, ou en mélange avec une autre.

**Conseil pratique** : ne pas oublier de nettoyer régulièrement le diffuseur (risque d’obturation des buses).

-----

###### 4/ En ambiance olfactive

Les huiles essentielles pures sont également utilisées pour le plaisir associé à leur parfum. Dans un diffuseur électrique par chaleur douce régulée, les molécules ne sont pas altérées par un chauffage intempestif et conservent toutes leurs qualités olfactives.

Dans ce mode d’utilisation, les huiles essentielles peuvent être utilisées en présence d’enfants de plus de 7 ans.

**Dans quel cas ?** C’est le moyen de parfumer agréablement votre intérieur.

**Comment ?** Ne se pratique jamais en continu. Rythme conseillé dans une pièce : 30 minutes deux à trois fois par jour.

Quantité par séquence : 5 à 10 gouttes.

**Conseil pratique** : ne pas laisser une huile essentielle plusieurs jours dans le diffuseur d’arômes car elle pourrait s’oxyder au contact de l’air.

-----

###### 5/ En inhalation

Les molécules aromatiques pénètrent aisément par les capillaires sanguins de la muqueuse nasale ; elles interviennent par la voie olfactive. L’inhalation humide s’utilise notamment pour le confort des voies aériennes supérieures. Cette voie est déconseillée pour un enfant de moins de 12 ans, ainsi qu’en cas d’asthme, de couperose, d’irritation du visage.

**Dans quel cas ?** Principalement pour le bien-être respiratoire et la relaxation.

**Inhalation humide** : verser de l’eau bouillante dans un inhalateur, en ajoutant 1 ou 2 gouttes d’huile essentielle ; respirer la vapeur pendant 5 minutes, en fermant les yeux.

**Inhalation sèche** : verser quelques gouttes sur un mouchoir et le placer sous le nez (ne pas s’en servir ensuite pour se moucher ou s’essuyer les yeux).

-----

###### 6/ En sauna facial

Pour les soins du visage (en particulier en cas de peaux grasses), on procède comme pour une inhalation humide, au-dessus d’un bol d’eau chaude.