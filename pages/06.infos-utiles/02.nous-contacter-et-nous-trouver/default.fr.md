---
title: 'Nous contacter et nous trouver'
twittercardoptions: summary
articleenabled: false
article:
    '@context': 'http://schema.org/'
    '@type': NewsArticle
    image:
        '@type': ImageObject
    publisher:
        '@type': Organization
        logo:
            '@type': ImageObject
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
    '@context': 'http://schema.org/'
    '@type': Restaurant
    address:
        '@type': PostalAddress
facebookenable: true
musicevent:
    '@context': 'http://schema.org/'
    '@type': MusicEvent
    location:
        '@type': MusicVenue
    offers:
        '@type': Offer
        priceCurrency: AED
person:
    '@context': 'http://schema.org/'
    '@type': person
    address:
        '@type': PostalAddress
event:
    '@context': 'http://schema.org/'
    '@type': Event
    location:
        '@type': Place
musicalbum:
    '@context': 'http://schema.org/'
    '@type': MusicAlbum
    byArtist:
        '@type': MusicGroup
---

# Nous contacter et nous trouver

Votre pharmacie est ouverte :
* Du **lundi au vendredi** de **8:30 à 12:30** et de **14:00 à 19:00**
* Le **samedi en continu** de **08:00 à 15:00**
  
Téléphone : [03.88.74.32.97](tel:0388743297)  
Fax : [03.88.74.14.01](fax:0388741401)  
Adresse : 19 Rue de Benfeld 67230 Huttenheim  
Email : [contact@pharmaciedehuttenheim.com](mailto:contact@pharmaciedehuttenheim.com)  
  
[fa=fa-facebook-square /] Retrouvez les dernières actualités de la pharmacie sur [notre page Facebook](https://www.facebook.com/pharmaciedehuttenheim)