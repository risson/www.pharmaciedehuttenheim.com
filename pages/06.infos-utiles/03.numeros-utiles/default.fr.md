---
title: 'Numéros utiles'
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
---

# Numéros utiles

### Numéros d'urgence
* Samu : [15](tel:15)
* Sapeurs-Pompiers : [18](tel:18)
* Gendarmerie : [17](tel:17)
* Centre Antipoison : [03.88.37.37.37](tel:0388373737)
* Enfance Maltraitée : [199](tel:199)
* Enfants disparus : [116 000](tel:116000)

### Liens utiles
* [Ordre national des pharmaciens](http://www.ordre.pharmacien.fr/)
* [Ministère des Solidarités et de la Santé](http://social-sante.gouv.fr/)
* [Agence nationale de sécurité du médicament et des produits de santé](http://ansm.sante.fr/)