---
title: 'Envoyez vos commentaires'
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
form:
    name: commentaires-form
    fields:
        -
            name: name
            label: 'Nom*'
            placeholder: 'Entrez votre nom'
            autofocus: 'on'
            autocomplete: 'on'
            type: text
            validate:
                required: true
        -
            name: email
            label: 'Email*'
            placeholder: 'Entrez votre adresse mail'
            type: email
            validate:
                required: true
        -
            name: subject
            label: Sujet
            placeholder: 'Entrez le sujet de votre message'
            type: text
            validate:
                required: false
        -
            name: message
            label: 'Message*'
            placeholder: 'Entrez votre message'
            type: textarea
            validate:
                required: true
        -
            name: g-recaptcha-response
            label: Captcha
            type: captcha
            recaptcha_not_validated: 'Le captcha n''est pas valide. Veuillez réessayer.'
            validate:
                required: true
    buttons:
        -
            type: submit
            value: Envoyer
        -
            type: reset
            value: Réinitialiser
    process:
        -
            email:
                from: '{{ config.plugins.email.from }}'
                to:
                    - '{{ config.plugins.email.to }}'
                    - '{{ form.value.email }}'
                subject: '[Commentaires | Pharmacie de Huttenheim] {{ form.value.subject }}'
                body: '{% include ''forms/data.html.twig'' %}'
        -
            save:
                fileprefix: feedback-
                dateformat: Ymd-His-u
                extension: txt
                body: '{% include ''forms/data.txt.twig'' %}'
                operation: create
        -
            message: 'Merci de vos commentaires !'
    reset: true
---

# Envoyez-nous vos commentaires !

*Les champs marqués d'une étoile sont obligatoires.*