---
title: 'Mentions légales'
visible: false
twitterenable: true
twittercardoptions: summary
articleenabled: false
musiceventenabled: false
orgaenabled: false
orga:
    ratingValue: 2.5
orgaratingenabled: false
eventenabled: false
personenabled: false
restaurantenabled: false
restaurant:
    acceptsReservations: 'yes'
    priceRange: $
facebookenable: true
---

# Mentions légales

Merci de lire attentivement les présentes modalités d'utilisation du présent site avant de le parcourir.  
En vous connectant sur ce site, vous acceptez sans réserve les présentes modalités.

**EDITEUR DU SITE**  
Pharmacie de Huttenheim  
19, Rue de Benfeld  
67230 Huttenheim  
France  
Tél. : 03.88.74.32.97  
[https://www.pharmaciedehuttenheim.com/](https://www.pharmaciedehuttenheim.com/)  
[contact@pharmaciedehuttenheim.com](mailto:contact@pharmaciedehuttenheim.com)

**CONDITIONS D'UTILISATION**  
Le site accessible par l'URL suivante : www.pharmaciedehuttenheim.com est exploité dans le respect de la législation française. L'utilisation de ce site est régie par les présentes conditions générales. En utilisant le site, vous reconnaissez avoir pris connaissance de ces conditions et les avoir acceptées. Celles-ci pourront êtres modifiées à tout moment et sans préavis par la société Pharmacie de Huttenheim.  
Pharmacie de Huttenheim ne saurait être tenu pour responsable en aucune manière d’une mauvaise utilisation du service.

**LIMITATION DE RESPONSABILITÉ**  
Les informations contenues sur ce site sont aussi précises que possibles et le site est périodiquement remis à jour, mais peut toutefois contenir des inexactitudes, des omissions ou des lacunes. Si vous constatez une lacune, erreur ou ce qui parait être un dysfonctionnement, merci de bien vouloir le signaler par [email](mailto:webmaster@pharmaciedehuttenheim.com) en décrivant le problème de la manière la plus précise possible (page posant problème, action déclenchante, type d’ordinateur et de navigateur utilisé, …).  
Tout contenu téléchargé se fait aux risques et périls de l'utilisateur et sous sa seule responsabilité. En conséquence, Pharmacie de Huttenheim ne saurait être tenu responsable d'un quelconque dommage subi par l'ordinateur de l'utilisateur ou d'une quelconque perte de données consécutives au téléchargement.  
Les photos sont non contractuelles.  
Les liens hypertextes mis en place dans le cadre du présent site internet en direction d'autres ressources présentes sur le réseau Internet ne sauraient engager la responsabilité de Pharmacie de Huttenheim.  

**LITIGES**  
Les présentes conditions sont régies par les lois françaises et toute contestation ou litiges qui pourraient naître de l'interprétation ou de l'exécution de celles-ci seront de la compétence exclusive des tribunaux dont dépend le siège social de la société Pharmacie de Huttenheim. La langue de référence, pour le règlement de contentieux éventuels, est le français.  

**DROIT D'ACCÈS**  
En application de cette loi, les internautes disposent d’un droit d’accès, de rectification, de modification et de suppression concernant les données qui les concernent personnellement. Ce droit peut être exercé par voie postale auprès de Pharmacie de Huttenheim 19, Rue de Benfeld 67230 Huttenheim France ou par voie électronique à l’adresse email suivante : [contact@pharmaciedehuttenheim.com](mailto:contact@pharmaciedehuttenheim.com).  
Les informations personnelles collectées ne sont en aucun cas confiées à des tiers hormis pour l’éventuelle bonne exécution de la prestation commandée par l’internaute.

**CONFIDENTIALITÉ**  
Vos données personnelles sont confidentielles et ne seront en aucun cas communiquées à des tiers hormis pour la bonne exécution de la prestation.

**PROPRIÉTÉ INTELLECTUELLE**  
Tout le contenu du présent site, incluant, de façon non limitative, les graphismes, images, textes, vidéos, animations, sons, logos, gifs et icônes ainsi que leur mise en forme sont la propriété exclusive de la société Pharmacie de Huttenheim à l'exception des marques, logos ou contenus appartenant à d'autres sociétés partenaires ou auteurs.  
Toute reproduction, distribution, modification, adaptation, retransmission ou publication, même partielle, de ces différents éléments est strictement interdite sans l'accord exprès par écrit de Pharmacie de Huttenheim. Cette représentation ou reproduction, par quelque procédé que ce soit, constitue une contrefaçon sanctionnée par les articles L.3335-2 et suivants du Code de la propriété intellectuelle. Le non-respect de cette interdiction constitue une contrefaçon pouvant engager la responsabilité civile et pénale du contrefacteur. En outre, les propriétaires des Contenus copiés pourraient intenter une action en justice à votre encontre.  
Pharmacie de Huttenheim est identiquement propriétaire des "droits des producteurs de bases de données" visés au Livre III, Titre IV, du Code de la Propriété Intellectuelle (loi n° 98-536 du 1er juillet 1998) relative aux droits d'auteur et aux bases de données.  
Les utilisateurs et visiteurs du site internet peuvent mettre en place un hyperlien en direction de ce site, mais uniquement en direction de la page d’accueil, accessible à l’URL suivante : [https://www.pharmaciedehuttenheim.com/](https://www.pharmaciedehuttenheim.com/), à condition que ce lien s’ouvre dans une nouvelle fenêtre. En particulier un lien vers une sous page (« lien profond ») est interdit, ainsi que l’ouverture du présent site au sein d’un cadre (« framing »), sauf l'autorisation expresse et préalable de Pharmacie de Huttenheim.  
Pour toute demande d'autorisation ou d'information, veuillez nous contacter par email : contact@pharmaciedehuttenheim.com. Des conditions spécifiques sont prévues pour la presse.
Par ailleurs, la mise en forme de ce site a nécessité le recours à des sources externes dont nous avons acquis les droits ou dont les droits d'utilisation sont ouverts :
* [Grav](https://getgrav.org/)

**HÉBERGEUR**  
OVH  
2 rue Kellermann  
59100 ROUBAIX  

### CONDITIONS DE SERVICE

Ce site est proposé en langages HTML5 et CSS3, pour un meilleur confort d'utilisation et un graphisme plus agréable, nous vous recommandons de recourir à des navigateurs modernes comme Firefox, Chrome, Opéra...

**INFORMATIONS ET EXCLUSION**  
Pharmacie de Huttenheim met en œuvre tous les moyens dont elle dispose, pour assurer une information fiable et une mise à jour fiable de ses sites internet. Toutefois, des erreurs ou omissions peuvent survenir. L'internaute devra donc s'assurer de l'exactitude des informations auprès de Natural-net, et signaler toutes modifications du site qu'il jugerait utile. Pharmacie de Huttenheim n'est en aucun cas responsable de l'utilisation faite de ces informations, et de tout préjudice direct ou indirect pouvant en découler.

**COOKIES**  
Pour des besoins de statistiques et d'affichage, le présent site utilise des cookies. Il s'agit de petits fichiers textes stockés sur votre disque dur afin d'enregistrer des données techniques sur votre navigation. Certaines parties de ce site ne peuvent être fonctionnelles sans l’acceptation de cookies.

**LIENS HYPERTEXTES**  
Le site internet de Pharmacie de Huttenheim peuvent offrir des liens vers d’autres sites internet ou d’autres ressources disponibles sur Internet.  
Pharmacie de Huttenheim ne dispose d'aucun moyen pour contrôler les sites en connexion avec son site internet. Pharmacie de Huttenheim ne répond pas de la disponibilité de tels sites et sources externes, ni ne la garantit. Elle ne peut être tenue pour responsable de tout dommage, de quelque nature que ce soit, résultant du contenu de ces sites ou sources externes, et notamment des informations, produits ou services qu’ils proposent, ou de tout usage qui peut être fait de ces éléments. Les risques liés à cette utilisation incombent pleinement à l'internaute, qui doit se conformer à leurs conditions d'utilisation.  
Les utilisateurs, les abonnés et les visiteurs du site internet de Pharmacie de Huttenheim ne peuvent mettre en place un hyperlien en direction de ce site sans l'autorisation expresse et préalable de Pharmacie de Huttenheim.  
Dans l'hypothèse où un utilisateur ou visiteur souhaiterait mettre en place un hyperlien en direction du site internet de Pharmacie de Huttenheim, il lui appartiendra d'adresser un email accessible sur le site afin de formuler sa demande de mise en place d'un hyperlien. Pharmacie de Huttenheim se réserve le droit d’accepter ou de refuser un hyperlien sans avoir à en justifier sa décision.

**RECHERCHE** 
En outre, le renvoi sur un site internet pour compléter une information recherchée ne signifie en aucune façon que Pharmacie de Huttenheim reconnaît ou accepte quelque responsabilité quant à la teneur ou à l'utilisation dudit site.

**PRÉCAUTIONS D'USAGE**
Il vous incombe par conséquent de prendre les précautions d'usage nécessaires pour vous assurer que ce que vous choisissez d'utiliser ne soit pas entaché d'erreurs voire d'éléments de nature destructrice tels que virus, trojans, etc....

**RESPONSABILITÉ**
Aucune autre garantie n'est accordée au client, auquel incombe l'obligation de formuler clairement ses besoins et le devoir de s'informer. Si des informations fournies par Pharmacie de Huttenheim apparaissent inexactes, il appartiendra au client de procéder lui-même à toutes vérifications de la cohérence ou de la vraisemblance des résultats obtenus. Pharmacie de Huttenheim ne sera en aucune façon responsable vis à vis des tiers de l'utilisation par le client des informations ou de leur absence contenues dans son site Internet.

**CONTACTEZ-NOUS**
Pharmacie de Huttenheim est à votre disposition pour tous vos commentaires ou suggestions. Vous pouvez nous écrire en français par courrier électronique à : [contact@pharmaciedehuttenheim.com](mailto:contact@pharmaciedehuttenheim.com).
