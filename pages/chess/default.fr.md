---
title: 'Échec et mat'
routable: true
twitterenable: true
twittercardoptions: summary
facebookenable: true
ssl: true
---

<!-- [fen]6K1/pN2R1PQ/p7/r2k3r/N2n4/1P2p3/BB5p/2Rb2bq w KQkq[/fen] -->

[fen]r1bk3r/p2pBpNp/n4n2/1p1NP2P/6P1/3P4/P1P1K3/q5b1 b KQkq[/fen]